## Poetry images

The goal of this repository is to provide images for Duniter client projects written in Python which uses Poetry.
There is currently Silkaj, and DuniterPy using them.

The images are based on the [official Python images](https://hub.docker.com/_/python?tab=description), based on latest Debian Slim, with [Poetry](https://python-poetry.org/), `libsodium`, and further development tools installed on top.
The goal is to provide all supported Python versions by the Python project.

